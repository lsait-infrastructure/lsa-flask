FROM registry.redhat.io/ubi7/ubi

RUN yum -y install --disableplugin=subscription-manager \
	httpd24 httpd24-httpd-devel rh-php72 rh-php72-php rh-python36-python rh-python36-python-pip \
	rh-python36-python-virtualenv gcc && yum --disableplugin=subscription-manager clean all

#RUN useradd -ms /bin/bash containeruser && echo "containeruser ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/containeruser && chmod 0440 /etc/sudoers.d/containeruser && echo "containeruser ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/containeruser && chmod 0440 /etc/sudoers.d/containeruser && usermod -aG wheel containeruser

COPY requirements.txt .
ENV APXS /opt/rh/httpd24/root/usr/bin/apxs

RUN /opt/rh/rh-python36/root/usr/bin/python3.6 -m virtualenv --python=/opt/rh/rh-python36/root/usr/bin/python3.6 /opt/venv
COPY hello.py  /opt/venv/
RUN chgrp -R 0 /opt/venv && \
    chmod -R g=u /opt/venv && \
    chmod -R 770 /opt/venv/bin 

RUN mkdir /etc/mod_wsgi-express-80 && \
    chmod -R g=u /etc/mod_wsgi-express-80 && \
    chmod -R 770 /etc/mod_wsgi-express-80

RUN . /opt/venv/bin/activate && pip install -r requirements.txt
RUN chmod 777 /var/run/
RUN sed -i 's/Listen 80/Listen 8080/' /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf && chgrp -R 0 /var/log/httpd24 /opt/rh/httpd24/root/var/run/httpd && chmod -R g=u /var/log/httpd24 /opt/rh/httpd24/root/var/run/httpd
EXPOSE 8080

CMD . /opt/venv/bin/activate && /opt/venv/bin/mod_wsgi-express start-server /opt/venv/hello.py --port=8080 --user apache --group root  --server-root=/etc/mod_wsgi-express-80 && /etc/mod_wsgi-express-80/apachectl start

